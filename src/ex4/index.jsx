import React, { useEffect, useState } from 'react';
import './ex4.css';
import SuperHero from './SuperHero';

// eslint-disable-next-line import/prefer-default-export
export const ExerciseFourView = () => {
  // eslint-disable-next-line no-unused-vars
  const [superheroes, setSuperheroes] = useState([]);

  useEffect(() => {
    fetch('https://cdn.jsdelivr.net/gh/akabab/superhero-api@0.3.0/api/all.json')
      .then((res) => res.json())
      .then((heroes) => setSuperheroes(heroes));
  });

  const filteredSuperheroes = superheroes.filter(superheroes.powerstats.power > 50);

  return (
    <p>
      <SuperHero name={filteredSuperheroes.name} alterEgo={filteredSuperheroes.biography.alterEgos} work={filteredSuperheroes.work.occupation} />
    </p>

  );
};
