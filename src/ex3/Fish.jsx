import React from 'react';

const Fish = (props) => {
  const body = '=';
  const queue = '><';
  const head = '°>';
  const { size } = props;
  return (
    <main>
      <p>{ queue.concat(body.repeat(size).concat(head)) }</p>
      <button type="button">-</button>
      <button type="button">+</button>
    </main>
  );
};

export default Fish;
