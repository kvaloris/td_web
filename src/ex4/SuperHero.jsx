import React from 'react';

const SuperHero = (props) => {
  const {
    name, alterEgo, work,
  } = props;
  return (
    <div>
      <h1>{name}</h1>
      <h2>{alterEgo}</h2>
      <h2>{work}</h2>
    </div>
  );
};

export default SuperHero;
